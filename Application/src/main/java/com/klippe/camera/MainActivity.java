/*
 * Copyright 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.klippe.camera;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;


public class MainActivity extends Activity {

    private Camera2PhotoFragment photoFragment;
    private Camera2VideoFragment videoFragment;
    private int STATE_CAMERA = 0;
    private ImageView btnCameraModeSwitch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnCameraModeSwitch = (ImageView) findViewById(R.id.camera_mode_switch);
        photoFragment = Camera2PhotoFragment.newInstance();
        if (null == savedInstanceState) {
            getFragmentManager().beginTransaction()
                    .replace(R.id.container, photoFragment)
                    .commit();
        }

        btnCameraModeSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment bundle=null;
                if (STATE_CAMERA == 0) {
                    bundle = Camera2VideoFragment.newInstance();
                    btnCameraModeSwitch.setImageResource(R.drawable.ic_video);
                    STATE_CAMERA=1;

                } else {
                    bundle = Camera2PhotoFragment.newInstance();
                    btnCameraModeSwitch.setImageResource(R.drawable.ic_photo);
                   STATE_CAMERA=0;

                }
                getFragmentManager().beginTransaction()
                        .setCustomAnimations(R.animator.change_fragment_in,R.animator.change_fragment_out)
                        .replace(R.id.container, bundle)
                        .commit();
            }
        });
    }

}
